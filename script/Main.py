from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon
import numpy as np
global a
import glob, os, os.path
import sys


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.setupUi(self)

    #set interface 
    def setupUi(self, MainWindow):
        #set style for frame 
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")


        self.right_frame = QtWidgets.QFrame(self)
        self.right_frame.setGeometry(QtCore.QRect(1200, 150, 250, 640)) #x, y, width, height
        self.right_frame.setStyleSheet("background-color: #FFFFFF;\n"
                                 "   border-radius: 16px;\n"
                                 "    border: 2px solid #6A4D32;")
        self.right_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.right_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.right_frame.setObjectName("frame")

        self.search_frame = QtWidgets.QFrame(self.centralwidget)
        self.search_frame.setGeometry(QtCore.QRect(350, 150, 800, 100)) #x, y, width, height
        self.search_frame.setStyleSheet("background-color: #FFFFFF;\n"
                                 "   border-radius: 16px;\n"
                                 "    border: 2px solid #6A4D32;")
        self.search_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.search_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.search_frame.setObjectName("frame2")

        self.titlelblsearch = QtWidgets.QLabel(self.search_frame)
        self.titlelblsearch.setGeometry(QtCore.QRect(16, 30, 700, 70))
        self.titlelblsearch.setAlignment(QtCore.Qt.AlignLeft)
        self.titlelblsearch.setObjectName("titlelblsearch")
        self.titlelblsearch.setStyleSheet("QFrame { border: 0px; background-color: transparent; font: 20pt;}")


        #set style for select image button
        self.slctimg = QtWidgets.QPushButton(self.search_frame)
        self.slctimg.setGeometry(QtCore.QRect(590, 20, 170, 60))
        self.slctimg.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.slctimg.setObjectName("slctimg")

        #set style for title 
        self.titlelblmenu = QtWidgets.QLabel(self.right_frame)
        self.titlelblmenu.setGeometry(QtCore.QRect(40, 24, 170, 60))
        self.titlelblmenu.setAlignment(QtCore.Qt.AlignCenter)
        self.titlelblmenu.setStyleSheet("QFrame { border: 0px; background-color: transparent; font: 20pt; }")
 
        self.titlelblmenu.setObjectName("titlelblmenu")

        #set style for median filter button
        self.medfil = QtWidgets.QPushButton(self.right_frame)
        self.medfil.setGeometry(QtCore.QRect(40, 116, 170, 60))
        self.medfil.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.medfil.setObjectName("medfil")

        #set style for gaussian filter button
        self.gausfil = QtWidgets.QPushButton(self.right_frame)
        self.gausfil.setGeometry(QtCore.QRect(40, 200, 170, 60))
        self.gausfil.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.gausfil.setObjectName("gausfil")
        
        #set style for thresholding button
        self.thres = QtWidgets.QPushButton(self.right_frame)
        self.thres.setGeometry(QtCore.QRect(40, 284, 170, 60))
        self.thres.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.thres.setObjectName("thres")

        #set style for dilation button
        self.dilate = QtWidgets.QPushButton(self.right_frame)
        self.dilate.setGeometry(QtCore.QRect(40, 368, 170, 60))
        self.dilate.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.dilate.setObjectName("dilate")

        #set style for morphology button
        self.morpho = QtWidgets.QPushButton(self.right_frame)
        self.morpho.setGeometry(QtCore.QRect(40, 452, 170, 60))
        self.morpho.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.morpho.setObjectName("morpho")

        #set style for save image button
        self.save = QtWidgets.QPushButton(self.right_frame)
        self.save.setGeometry(QtCore.QRect(40, 544, 170, 60))
        self.save.setStyleSheet("color:white;\n"
                                   "font: 14pt \"Gadugi\";\n"
                                   "   border-radius: 16px;\n"
                                   "    border: 2px solid #6A4D32;\n"
                                   "background-color:#7AA75F;\n"
                                   "width:171;\n"
                                   "height:61")
        self.save.setObjectName("save")

        #set style for original image title
        self.titleoriginalimage = QtWidgets.QLabel(self.centralwidget)
        self.titleoriginalimage.setGeometry(QtCore.QRect(420, 282, 700, 70))
        self.titleoriginalimage.setStyleSheet("font: 18pt \"Montserrat Bold\";\n"
                                    "color: #000;")
        self.titleoriginalimage.setObjectName("titleoriginalimage")

        #set style for ouput image title 
        self.titleoutputimage = QtWidgets.QLabel(self.centralwidget)
        self.titleoutputimage.setGeometry(QtCore.QRect(880, 282, 700, 70))
        self.titleoutputimage.setStyleSheet("font: 18pt \"Montserrat Bold\";\n"
                                    "color: #000;")
        self.titleoutputimage.setObjectName("titleoutputimage")

        #set style for original image 
        self.originalimagebox = QtWidgets.QLabel(self.centralwidget)
        self.originalimagebox.setGeometry(QtCore.QRect(350, 368, 360, 425))
        self.originalimagebox.setStyleSheet("background-color: #FFF;\n"
                                 "   border-radius: 20px;\n"
                                 "    border: 2px solid #7AA75F;")
        self.originalimagebox.setText("")
        self.originalimagebox.setObjectName("originalimagebox")

        #set style for filtering image
        self.outputbox = QtWidgets.QLabel(self.centralwidget)
        self.outputbox.setGeometry(QtCore.QRect(790, 368, 360, 425))
        self.outputbox.setStyleSheet("background-color: #FFF;\n"
                                   "   border-radius: 20px;\n"
                                   "    border: 2px solid #7AA75F;")
        self.outputbox.setText("")
        self.outputbox.setObjectName("outputbox")

        #set style for title 
        self.titlelbl = QtWidgets.QLabel(self.centralwidget)
        self.titlelbl.setGeometry(QtCore.QRect(550, 32, 700, 70))
        self.titlelbl.setAlignment(QtCore.Qt.AlignCenter)
        font = QtGui.QFont()
        font.setFamily("Montserrat Bold")
        font.setPointSize(26)
        font.setWeight(50)
        self.titlelbl.setFont(font)
        self.titlelbl.setStyleSheet("font: 26pt \"Montserrat Bold\";\n"
                                    "color: #000;")
        self.titlelbl.setObjectName("titlelbl")


        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        error_dialog = QtWidgets.QErrorMessage()
        error_dialog.setWindowTitle('Informasi')
        error_dialog.showMessage('Silahkan pilih gambar terlebih dahulu dan jalankan fitur sesuai urutan yang terdapat pada menu')
        error_dialog.exec()

        self.slctimg.clicked.connect(self.setImage)   
        self.medfil.clicked.connect(self.medianfilter)   
        self.gausfil.clicked.connect(self.gaussianfilter)   
        self.thres.clicked.connect(self.threshing)   
        self.dilate.clicked.connect(self.dilation)   
        self.morpho.clicked.connect(self.morphology)   
        self.save.clicked.connect(self.saving)   
        

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "UI - Image Processing"))
        self.slctimg.setText(_translate("MainWindow", "Browse"))
        self.medfil.setText(_translate("MainWindow", "Median Filter"))
        self.gausfil.setText(_translate("MainWindow", "Gausian Filter"))
        self.thres.setText(_translate("MainWindow", "Thresholding"))
        self.dilate.setText(_translate("MainWindow", "Dilation"))
        self.morpho.setText(_translate("MainWindow", "Morphology"))
        self.save.setText(_translate("MainWindow", "Save Image"))
        self.titlelbl.setText(_translate("MainWindow", "UI - Image Processing"))
        self.titlelblmenu.setText(_translate("MainWindow", "Menu"))
        self.titlelblsearch.setText(_translate("MainWindow", "Pilih Gambar"))
        self.titleoriginalimage.setText(_translate("MainWindow", "Original Image"))
        self.titleoutputimage.setText(_translate("MainWindow", "Output Image"))

    def setImage(self):

        global a
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Select Image", "C://Users/HASSAN/OneDrive/Finaldata/",
                                                            "Image Files (*.png *.jpg *jpeg *.bmp)")  # Ask for file

        a = fileName

        if fileName:  # If the user gives a file

            pixmap = QtGui.QPixmap(fileName)  # Setup pixmap with the provided image
            pixmap = pixmap.scaled(self.originalimagebox.width(), self.originalimagebox.height(),
                                   QtCore.Qt.KeepAspectRatio)  # Scale pixmap
            self.originalimagebox.setPixmap(pixmap)  # Set the pixmap onto the label
            self.originalimagebox.setAlignment(QtCore.Qt.AlignCenter)  # Align the label to center

        print(fileName)
    
    def medianfilter(self):
        import cv2
        import os

        img = cv2.imread(a)
        path = "output/temp"

        median = cv2.medianBlur(img, 5)

        cv2.imwrite(os.path.join(path, 'median.png'), median)

        pixmap = QtGui.QPixmap(os.path.join(path, 'median.png'))  # Setup pixmap with the provided image

        pixmap = pixmap.scaled(self.outputbox.width(), self.outputbox.height(),
                               QtCore.Qt.KeepAspectRatio)  # Scale pixmap
        self.outputbox.setPixmap(pixmap)  # Set the pixmap onto the label

    def gaussianfilter(self):
        import cv2
        import os

        path = "output/temp"
        img = cv2.imread(os.path.join(path, "median.png"))

        equ1 = cv2.GaussianBlur(img, (5, 5), cv2.BORDER_CONSTANT)

        cv2.imwrite(os.path.join(path, 'gaussian.png'), equ1)

        pixmap = QtGui.QPixmap(os.path.join(path, 'gaussian.png'))  # Setup pixmap with the provided image
        pixmap = pixmap.scaled(self.outputbox.width(), self.outputbox.height(),
                               QtCore.Qt.KeepAspectRatio)  # Scale pixmap
        self.outputbox.setPixmap(pixmap)  # Set the pixmap onto the label

    def threshing(self):
        import cv2
        import os

        path = "output/temp"
        img = cv2.imread(os.path.join(path, "gaussian.png"))

        ret, thresh1 = cv2.threshold(img, 127, 190, cv2.ADAPTIVE_THRESH_GAUSSIAN_C)

        cv2.imwrite(os.path.join(path, 'thresh.png'), thresh1)

        pixmap = QtGui.QPixmap(os.path.join(path, 'thresh.png'))  # Setup pixmap with the provided image
        pixmap = pixmap.scaled(self.outputbox.width(), self.outputbox.height(),
                               QtCore.Qt.KeepAspectRatio)  # Scale pixmap
        self.outputbox.setPixmap(pixmap)  # Set the pixmap onto the label

    

    def dilation(self):
        import cv2
        import os

        path = "output/temp"
        img = cv2.imread(os.path.join(path, "thresh.png"))

        kernel = np.ones((5, 5), np.uint8)
        opening1 = cv2.dilate(img, kernel, iterations=0)
        opening2 = cv2.morphologyEx(opening1, cv2.MORPH_CLOSE, kernel)

        TE=cv2.imwrite(os.path.join(path, 'dilated.png'), opening2)

        pixmap = QtGui.QPixmap(os.path.join(path, 'dilated.png'))  # Setup pixmap with the provided image

        pixmap = pixmap.scaled(self.outputbox.width(), self.outputbox.height(),
                               QtCore.Qt.KeepAspectRatio)  # Scale pixmap
        self.outputbox.setPixmap(pixmap)  # Set the pixmap onto the label

    def morphology(self):
        import cv2
        import os

        img = cv2.imread(a)
        grey = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        path = "output/temp"
        dilate = cv2.imread(os.path.join(path, "dilated.png"))
        grey2 = cv2.cvtColor(dilate, cv2.COLOR_RGB2GRAY)


        contours, hierarchy = cv2.findContours(grey2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        b = cv2.drawContours(grey, contours, -1, (0, 0, 255), 3)

        dst = cv2.addWeighted(grey2, 0.7, b, 0.3, 0)

        cv2.imwrite(os.path.join(path, 'morphology.png'), dst)

        pixmap = QtGui.QPixmap(os.path.join(path, 'morphology.png'))  # Setup pixmap with the provided image
        pixmap = pixmap.scaled(self.outputbox.width(), self.outputbox.height(),
                               QtCore.Qt.KeepAspectRatio)  # Scale pixmap
        self.outputbox.setPixmap(pixmap)  # Set the pixmap onto the label    

    
    def saving(self):
        import cv2
        import os
        from PyQt5 import QtWidgets
        import glob, os, os.path

        path="output/temp"

        img = cv2.imread(os.path.join(path, 'morphology.png'))

        fname,_ = QtWidgets.QFileDialog.getSaveFileName(None, 'Open file',
                                                        'output/temp', "Image files (*.jpg)")

        if fname:
            cv2.imwrite(fname, img)
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.setWindowTitle('Image processing tool')
            error_dialog.showMessage('Image Saved')
            error_dialog.exec()

            self.originalimagebox.clear()
            self.outputbox.clear()
            filelist=glob.glob(os.path.join(path,"*.png"))
            for f in filelist:
                os.remove(f)
                print('deleted')

        else:
            print('notsaved')
            error_dialog = QtWidgets.QErrorMessage()
            error_dialog.setWindowTitle('Image processing tool')
            error_dialog.showMessage('Please save image!')
            error_dialog.exec()
            self.originalimagebox.clear()
            self.outputbox.clear()
            filelist = glob.glob(os.path.join(path, "*.png"))
            for f in filelist:
                os.remove(f)

    


def main():
    app = QtWidgets.QApplication(sys.argv)
    #set style for main background
    main_stylesheet = """
        QMainWindow  {
            background-image: url("asset/background.png"); 
            background-repeat: no-repeat; 
            background-position: center;
        }
    """

    app.setStyleSheet(main_stylesheet)
    ui = Ui_MainWindow()

     #set ui size
    ui.resize(1700, 900)
    ui.showNormal


    #show ui
    ui.show()
    sys.exit(app.exec_())



if __name__ == "__main__":
    main()
   